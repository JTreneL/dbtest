import sqlite3
import sys
import pkg_resources 


dbcars = pkg_resources.resource_string(__name__, "data/cars.db")
conn = sqlite3.connect(dbcars)
cursor = conn.cursor()
cursor.execute('CREATE TABLE IF NOT EXISTS cars(znacka TEXT, rok NUMERIC, model TEXT, id NUMERIC, info TEXT)')


def delete():
    delinput = input("ENTER ID TO DELETE:")
    deleting = "DELETE FROM cars WHERE id = ?"
    cursor.execute(deleting,delinput)
    conn.commit()
    print("CAR DELETED!")
    main()


def putdinfo(znacka, rok,model,id):
    put = '''INSERT INTO cars(znacka, rok, model, id)VALUES (?, ?, ?, ?)'''
    cursor.execute(put,[(znacka), (rok), (model),(id)])
    conn.commit()
    

def addinfo():
    id = input("Enter ID of Car:")
    info = input("Enter info for the car:")
    data = (info,id)
    update = "UPDATE cars SET info = ? where id= ?"
    cursor.execute(update,data)
    print("INFO STORED!")
    conn.commit()
    main()


def idplus():
    cursor.execute("SELECT id FROM cars")
    listid = cursor.fetchall()
    listid = list(listid)
    idchange = (max(listid)[0])
    idchange += 1
    return idchange


def insertcars():
    znacka = input ("MANUFACTURER: ")
    rok = input ("YEAR: ")
    model = input ("MODEL: ")
    id = idplus(cursor)
    putdinfo(znacka, rok,model,id,cursor,conn)
    print("CAR SAVED!")
    main()


def main():
    while True:
        menu1 = input("1: CRT DB 2: FETCHALL 3: ADD INFO BY ID 4: DELETE e: EXIT 6.INSERT CAR:  ")
        if menu1 == "1":
            putdinfo("Audi", 2006,"A6", 1,)
            putdinfo("Audi", 2007,"A7", 2,)
            putdinfo("Audi", 2009,"A3", 3,)
            putdinfo("Audi", 2004,"A5", 4,)
            putdinfo("Audi", 2007,"A4", 5,)
            main()
        elif menu1 == "2":
            cursor.execute("SELECT * from cars")
            text = cursor.fetchall()
            print(text)
            main()
        elif menu1 == "3":
            addinfo()
        elif menu1 =="4":
            delete()
        elif menu1 == "e":
            quit()
        elif menu1 == "6":
            insertcars()
        else:
            print("WRONG INPUT")


if __name__ == "__main__":
    sys.exit(main())
