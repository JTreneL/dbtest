# Cars database:
- Database with cars in Sqlite3
- Can add info 
- Can delete cars 

# Install

- extract all files to new directory
- open console and go to directory
- enter ./install.sh


# Uninstall

- open console and go to directory
- enter ./uninstall.sh


# How to Run it:

- open console
- enter [username@fedora ~]$ carsdb


# License:

- opensource